#include "Arduino_LSM6DS3.h"
#include "MadgwickAHRS.h"

#define SAMPLE_RATE 100

Madgwick filter;


///the calibration factors for angular acceleration measurements
static float gx_calibration, gy_calibration, gz_calibration;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  delay(1000); //relax...
  Serial.println("Processor came out of reset.\n");
  
  //Call .begin() to configure the IMU
  IMU.begin();
  filter.begin(SAMPLE_RATE);
  ///initialize the calibration factor
  IMU.readGyroscope(gx_calibration, gy_calibration, gz_calibration);
}


void loop()
{
  static unsigned long previous_time = millis();
  unsigned long current_time = millis();
  
  if (current_time - previous_time >= 1000/SAMPLE_RATE)
  {
    ///only record the YAW
    float z_angle = compute_angle();
    Serial.print(z_angle);
    Serial.print("\n");
    previous_time = millis();
  }
}

float compute_angle()
{
  float ax, ay, az;
  float gx, gy, gz;

  IMU.readAcceleration(ax,ay,az);
  IMU.readGyroscope(gx,gy,gz);

  if(gz <=1.5 * abs(gz_calibration) && gz >= -1.5*abs(gz_calibration))
    gz =0;

  ///filter is an API which can do integration
  filter.updateIMU(gx,gy,gz,ax,ay,az);
  return filter.getYaw();
}
