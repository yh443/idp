#include "bluetooth_headers.h"
#define leftLineSensorPin A2
#define centreLineSensorPin A0
#define rightLineSensorPin A1


inline void initSensors(){
  pinMode(leftLineSensorPin, INPUT);
  pinMode(centreLineSensorPin, INPUT);
  pinMode(rightLineSensorPin, INPUT);
}

linePosition getLinePosition(){
  static constexpr uint16_t threshold = 740;
  bool left_on_line = analogRead(leftLineSensorPin) < threshold;
  bool centre_on_line = analogRead(centreLineSensorPin) < threshold;
  bool right_on_line = analogRead(rightLineSensorPin) < threshold;
  if (centre_on_line){
    if (left_on_line&& right_on_line) return linePosition::all_3;
    else if (left_on_line) return linePosition::slightly_right;
    else if (right_on_line) return linePosition::slightly_left;
    else return linePosition::centre;
  }
  
  else{
    if (left_on_line&& right_on_line) return linePosition::outer_2;
    else if (left_on_line) return linePosition::far_right;
    else if (right_on_line) return linePosition::far_left;
    else return linePosition::off_line;
  }  
}

void followLine(){
  while(true){
    if(millis() - last_time > LED_HALF_PERIOD) {
    last_time = millis();
    toggle_led();
    }
    // if python has another message for you, don't go back to following the line
    if(SerialNina.available()) {return;}
    {
      static linePosition robot_position = getLinePosition();
      static linePosition previous_robot_position;
      previous_robot_position = robot_position;
      robot_position = getLinePosition();
      switch(robot_position){
        case linePosition::centre:
          goForward(255);
          break;
        case linePosition::slightly_left:
          turnRight(255);
          break;
        case linePosition::slightly_right:
          turnLeft(255);
          break;
        case linePosition::far_left:
          rotateRight(255);
          break;
        case linePosition::far_right:
          rotateLeft(255);
          break;
        case linePosition::off_line:
          if(previous_robot_position == linePosition::far_right || previous_robot_position == linePosition::slightly_right)
            rotateLeft(255);
          else if (previous_robot_position == linePosition::far_left || previous_robot_position == linePosition::slightly_left)
            rotateRight(255);
          else
            // should get help from python as this might make the robot turn in the wrong direction
            rotateRight(255);
        case linePosition::outer_2:
          rotateRight(255);
      }
    }
  }
}

void go_to_start(){
  displace_robot(0, 100, true);
  displace_robot(180, 0, false);
  followLine();
  
}
