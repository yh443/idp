#include "bluetooth_headers.h"
#include <Adafruit_MotorShield.h>
#include "Arduino_LSM6DS3.h"
#include "MadgwickAHRS.h"
#include "data.h"
float gx0, gy0, gz0;

Adafruit_MotorShield AFMS = Adafruit_MotorShield();

void setup() {  
  pinMode(green_led, OUTPUT);
  pinMode(red_led, OUTPUT);
  pinMode(LDR1_PIN, INPUT);
  pinMode(LDR2_PIN, INPUT);
  Serial.begin(74800);
  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");
    while (true);
  }
  delay(1000);
  int max_ittr = 500;
  digitalWrite(green_led, HIGH);
  for(int16_t i = 0; i < max_ittr; i++){
    while(!IMU.gyroscopeAvailable());
    static float gx, gy, gz;
    IMU.readGyroscope(gx, gy, gz);
    gx0 += gx; gy0 += gy; gz0 += gz;
  }
  digitalWrite(green_led, LOW);
  
  gx0/=float(max_ittr);
  gy0/=float(max_ittr);
  gz0/=float(max_ittr);
  filter.begin(SAMPLE_RATE);
  last_time = millis();
  pinMode(AMBER_PIN, OUTPUT);
  AFMS.begin();
  
  pinMode(NINA_RESETN, OUTPUT);         
  digitalWrite(NINA_RESETN, LOW);
  SerialNina.begin(115200);

  ldr1_threshold = 0.0;
  for (int i = 0; i < 10; i++){
    ldr1_threshold += analogRead(LDR1_PIN) * 5.0 / 1024.0;
    delay(10);
  }
  ldr1_threshold /= 10.0;


  ldr2_threshold = 0.0;
  for (int i = 0; i < 10; i++){
    ldr2_threshold += analogRead(LDR2_PIN) * 5.0 / 1024.0;
    delay(10);
  }
  ldr2_threshold /= 10.0;
  Serial.println(ldr1_threshold);
  Serial.println(ldr2_threshold);
  if (SerialNina.find("!\r\n")) Serial.println("Ready for python connection");
  else {
    Serial.println("Failed to initialize bluetooth!");
    while(true);
  }
}

void loop() {
  // If there is available data from Bluetooth, read the data and 
  // make the necessary changes. This should be as fast as possible
  if(millis() - last_time > LED_HALF_PERIOD) {
    last_time = millis();
    toggle_led();
  }
  //followLine();
  //turnLeft(255);
  //displace_robot(90, 100);
  /*if (Serial.available()){
    Serial.read();
    //go_to_start();
    //check_fruit();
  }*/
  if (SerialNina.available()) dispatch_message();  
}
