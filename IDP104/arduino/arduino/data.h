#pragma once
#define LDR1_PIN A3
#define LDR2_PIN A4
//define the global variables here
#define SAMPLE_RATE 100.0
Madgwick filter;

///the calibration factors for angular acceleration measurements
float gx_calibration, gy_calibration, gz_calibration;

int amber_led = 4;
int green_led = 3;
int red_led = 2;

int ldr_1 = A0;
int ldr_2 = A1;
