#include "data.h"
constexpr float microsPerReading = 1e6 / SAMPLE_RATE;

float rotate(float angle, bool about_centre, bool back = false){
  static float microsPrevious;
  static float ax, ay, az;
  static float gx, gy, gz;
  static float roll, pitch, heading;
  static unsigned long microsNow;
  float initial_heading = filter.getYaw();
  angle/= 1.146;
  // check if it's time to read data and update the filter
  microsPrevious = micros();
  while(1){
   if(millis() - last_time > LED_HALF_PERIOD) {
    toggle_led();
    last_time = millis();
    }
    microsNow = micros();
    if (microsNow - microsPrevious >= microsPerReading) {
      IMU.readGyroscope(gx, gy, gz);
      IMU.readAcceleration(ax, ay, az);
      // update the filter, which computes orientation
      filter.updateIMU(gx - gx0, gy - gy0, gz - gz0, ax, ay, az);
      
      // print the heading, pitch and roll
      roll = filter.getRoll();
      pitch = filter.getPitch();
      heading = filter.getYaw();
      float imu_angle_travelled = initial_heading - heading;
      while(imu_angle_travelled < -180.0) imu_angle_travelled+=360.0;
      while(imu_angle_travelled > 180.0) imu_angle_travelled-=360.0;
      float angle_error = angle + (initial_heading - heading);
      while(angle_error < -180.0) angle_error+=360.0;
      while(angle_error > 180.0) angle_error-=360.0;
      bool _spin_left = angle_error > 0.0;
      static constexpr byte min_power = 180;
      // rotation speed at min_power must be less than 20 degrees per second (18 second period)
      byte motor_power = constrain(7.0 * abs(angle_error) + min_power, 0, 255);
      if(!back){
        if(!about_centre)
        if(_spin_left) rotateLeft(motor_power);
        else rotateRight(motor_power);
      else
        if(_spin_left) spinLeft(motor_power);
        else spinRight(motor_power); 
      }
      else{
        if(!about_centre)
        if(_spin_left) rotateRightBack(motor_power);
        else rotateLeftBack(motor_power);
      else
        if(_spin_left) spinRightBack(motor_power);
        else spinLeftBack(motor_power); 
      }
        
      Serial.println(angle_error);
      //Serial.print("  Motor power: "); Serial.print(motor_power); Serial.print(_spin_left ? " (left)" : " (right)");
      // increment previous time, so we keep proper pace
      if(fabsf(angle_error) < 0.1){
        goForward(0);
        return angle_error;
      }
      microsPrevious += microsPerReading;
    }
  }
}

void displace_robot(int16_t angle, int16_t _distance, bool back){
  float distance = 0.41667 * _distance;
  rotate(angle, /*about centre*/ true);
  static constexpr int16_t half_wheel_separation = 105;
  float total_absolute_angle = radians(distance / half_wheel_separation);
  float distance_travelled = 0.0;
  static constexpr float max_angle = 1.0;
  if (distance > radians(max_angle/2.0) * half_wheel_separation) {rotate(max_angle/2.0, false); distance_travelled = radians(max_angle/2.0) * half_wheel_separation;}
  bool left = false;
  while(distance - distance_travelled > radians(max_angle) * half_wheel_separation){
    rotate(left ? max_angle : -max_angle, false, back);
    left = !left;
    distance_travelled += radians(max_angle) * half_wheel_separation;
  }
  Serial.println();
  Serial.println(distance_travelled);
  rotate((left ? 1.0 : -1.0) * degrees((distance - distance_travelled)/ half_wheel_separation), false, back);
}

void find_fruit(int16_t angle, uint16_t distance){
  displace_robot(angle, distance);
  SerialNina.write((byte)bluetooth_header::check_pose);
  Serial.println("Check pose command sent...");
}

void find_line(int16_t angle, uint16_t distance){
  displace_robot(angle, distance);
  rotate(15, /*about centre*/ true);
  struct line_pos_msg_t{
    const bluetooth_header header = bluetooth_header::line_status;
    line_status status;
  } line_pos_msg;
  
  for (int i = 0; i < 12; i++){
    if (getLinePosition() == linePosition::off_line) rotate(-5, /*about centre*/ true); // rotate -5 degrees
    else {
      line_pos_msg.status = line_status::on_line;
      SerialNina.write((uint8_t*)(void*)&line_pos_msg, sizeof(line_pos_msg));
      return;
    }
  }
  line_pos_msg.status = line_status::off_line;
  SerialNina.write((uint8_t*)(void*)&line_pos_msg, sizeof(line_pos_msg));
}


inline float compute_angle(float gz_calibration)
{
  float ax, ay, az;
  float gx, gy, gz;

  IMU.readAcceleration(ax,ay,az);
  IMU.readGyroscope(gx,gy,gz);

  if(gz <=1.5 * abs(gz_calibration) && gz >= -1.5*abs(gz_calibration))
    gz =0;

  ///filter is an API which can do integration
  filter.updateIMU(gx,gy,gz,ax,ay,az);
  return filter.getYaw();
}

int record_angle(float gz_calibration)
{
  static unsigned long previous_time = millis();
  unsigned long current_time = millis();
  float z_angle;
  if (current_time - previous_time >= 1000/SAMPLE_RATE)
  {
    ///only record the YAW
    float z_angle = compute_angle(gz_calibration);
    Serial.print(z_angle);
    Serial.print("\n");
    previous_time = millis();
  }

  return int(z_angle);
}
