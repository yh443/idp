#define MOTOR_BACKWARD FORWARD
#define MOTOR_FOREWARD BACKWARD
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1), *rightMotor = AFMS.getMotor(2);
uint8_t leftMotorSpeed = 0, rightMotorSpeed = 0;
bool leftMotorForward = true, rightMotorForward = true; // never release the motors

void goForward(uint8_t _speed){
  
  leftMotor->setSpeed(_speed);
  rightMotor->setSpeed(_speed);
  leftMotor->run(MOTOR_FOREWARD);
  rightMotor->run(MOTOR_FOREWARD);
  
  leftMotorForward = true;
  rightMotorForward = true;
  leftMotorSpeed = _speed;
  rightMotorSpeed = _speed;
}

void turnRight(uint8_t _speed){ 
  leftMotor->setSpeed(_speed);
  rightMotor->setSpeed(_speed * 0.5f);
  leftMotor->run(MOTOR_FOREWARD);
  
  leftMotorForward = true;
  leftMotorSpeed = _speed;
  rightMotorSpeed = 0;
}

void turnLeft(uint8_t _speed){
  leftMotor->setSpeed(_speed * 0.5f);
  rightMotor->setSpeed(_speed);
  rightMotor->run(MOTOR_FOREWARD);
  
  rightMotorForward = true;
  leftMotorSpeed = 0;
  rightMotorSpeed = _speed;
}

void rotateRight(uint8_t _speed){ 
  leftMotor->setSpeed(_speed);
  rightMotor->setSpeed(0);
  leftMotor->run(MOTOR_FOREWARD);
  rightMotor->run(MOTOR_BACKWARD);
  
  leftMotorForward = true;
  rightMotorForward = false;
  leftMotorSpeed = _speed;
  rightMotorSpeed = _speed;
}

void rotateLeft(uint8_t _speed){ 
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(_speed);
  rightMotor->run(MOTOR_FOREWARD);
  leftMotor->run(MOTOR_BACKWARD);
  
  leftMotorForward = true;
  rightMotorForward = false;
  leftMotorSpeed = _speed;
  rightMotorSpeed = _speed;
}

void spinLeft(uint8_t _speed){
  leftMotor->setSpeed(_speed);
  rightMotor->setSpeed(_speed);
  leftMotor->run(MOTOR_BACKWARD);
  rightMotor->run(MOTOR_FOREWARD);
  
  leftMotorForward = false;
  rightMotorForward = true;
  leftMotorSpeed = _speed;
  rightMotorSpeed = _speed;
}

void spinRight(uint8_t _speed){ 
  leftMotor->setSpeed(_speed);
  rightMotor->setSpeed(_speed);
  leftMotor->run(MOTOR_FOREWARD);
  rightMotor->run(MOTOR_BACKWARD);
  
  leftMotorForward = true;
  leftMotorSpeed = _speed;
  rightMotorSpeed = _speed;
}

void rotateRightBack(uint8_t _speed){ 
  leftMotor->setSpeed(_speed);
  rightMotor->setSpeed(0);
  leftMotor->run(MOTOR_BACKWARD);
  rightMotor->run(MOTOR_BACKWARD);
  
  leftMotorForward = true;
  rightMotorForward = false;
  leftMotorSpeed = _speed;
  rightMotorSpeed = _speed;
}

void rotateLeftBack(uint8_t _speed){ 
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(_speed);
  rightMotor->run(MOTOR_BACKWARD);
  leftMotor->run(MOTOR_BACKWARD);
  
  leftMotorForward = true;
  rightMotorForward = false;
  leftMotorSpeed = _speed;
  rightMotorSpeed = _speed;
}

void spinLeftBack(uint8_t _speed){
  leftMotor->setSpeed(_speed);
  rightMotor->setSpeed(_speed);
  leftMotor->run(MOTOR_FOREWARD);
  rightMotor->run(MOTOR_BACKWARD);
  
  leftMotorForward = false;
  rightMotorForward = true;
  leftMotorSpeed = _speed;
  rightMotorSpeed = _speed;
}

void spinRightBack(uint8_t _speed){ 
  leftMotor->setSpeed(_speed);
  rightMotor->setSpeed(_speed);
  leftMotor->run(MOTOR_BACKWARD);
  rightMotor->run(MOTOR_FOREWARD);
  
  leftMotorForward = true;
  leftMotorSpeed = _speed;
  rightMotorSpeed = _speed;
}
