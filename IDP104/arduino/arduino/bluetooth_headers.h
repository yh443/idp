// Define the types of message that can be sent
// This comes at the start of every message so the Arduino knows how to interpret the message
#pragma once
#define AMBER_PIN 4
#define LED_HALF_PERIOD 250
static unsigned long last_time = 0;

enum class line_status : uint8_t{
  on_line = 0,
  off_line
};

enum class fruit_status : uint8_t{
  ripe = 0,
  unripe
};
enum class bluetooth_header : uint8_t {
  stop = 0, // this has no body and is equivalent to the message {go_forward, (uint8_t)0}
  follow_line, // this has no body but the Arduino must be able to respond timely to a stop command
  check_fruit, // this has no body. It just means that python is happy that the robot is facing the fruit at an acceptable angle and distance
  find_fruit, // followed by {int16_t signed_angle /*in degrees*/, uint16_t distance /*in mm*/}
  find_line, // followed by {int16_t signed_angle /*in degrees*/, uint16_t distance /*in mm*/}
  
  check_pose, // not followed by anything, just a request for python to check if the robot is facing the fruit
  line_status, // followed by 1 byte enum (on-line, off-line)
  fruit_status, // followed by the fruit's status (1 byte enum (ripe/unripe))
  go_to_start
};

enum class linePosition : uint8_t{
  centre,
  slightly_left,
  slightly_right,
  far_left,
  far_right,
  off_line,
  all_3,
  outer_2
};

void toggle_led(){
  static bool led_on = false;
  if (led_on) digitalWrite(AMBER_PIN, LOW);
  else digitalWrite(AMBER_PIN, HIGH);
  led_on = !led_on;
}

void displace_robot(int16_t angle, int16_t distance, bool back = false);

static float ldr1_threshold, ldr2_threshold;
