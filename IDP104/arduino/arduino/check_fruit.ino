void collect_fruit(){
  // go forawrd 10 cm
  displace_robot(/*angle*/ 0, /*distance*/ 150);
}

void avoid_fruit(){
  displace_robot(/*angle*/ 0, /*distance*/ 55, /*back*/ true);
  displace_robot(/*angle*/ 90, /*distance*/100);
}


fruit_status check_fruit()
{
  digitalWrite(amber_led, HIGH);
  delay(1000);
  digitalWrite(amber_led, LOW);
  displace_robot(0, 50);
  static constexpr float threshold = 2.8;
  float ldr1_value = analogRead(LDR1_PIN) * 5.0 / 1024.0;
  float ldr2_value = analogRead(LDR2_PIN) * 5.0 / 1024.0;
  bool unripe = ldr1_value - ldr1_threshold > 0.0 && ldr2_value - ldr2_threshold > 0.0;
  if (!unripe)
  {
    Serial.println("GREEN LED ON");
    digitalWrite(green_led, HIGH);
    delay(5000);
    digitalWrite(green_led, LOW);
    collect_fruit();
    return fruit_status::ripe;
  }
  else
  {
    digitalWrite(red_led, HIGH);
    delay(5000);
    digitalWrite(red_led, LOW);
    avoid_fruit();
    return fruit_status::unripe;
  }
}
