#include "data.h"
inline void setupBluetooth(uint16_t baud_rate){
  // start the Bluetooth connection
  pinMode(NINA_RESETN, OUTPUT);                
  digitalWrite(NINA_RESETN, LOW);
  // Set the baud rate for the Bluetooth to 115200
  SerialNina.begin(baud_rate);
  // Keep reading until you reach the end of the message
}

void dispatch_message(){
  // Read the header to know which type of message is being sent
  static bluetooth_header header;
  static uint8_t _speed;
  SerialNina.readBytes((byte*)(void*)&header, sizeof(header));
  switch(header){
    case bluetooth_header::stop:
    {
      Serial.println("Stop command recieved");
      goForward(0);
      break;
    }
    
    case bluetooth_header::follow_line:
    {
      // follow the line
      Serial.println("Follow line command recieved");
      followLine();
      break;
    }
    
    case bluetooth_header::check_fruit:
    {
      Serial.println("Check fruit command recieved");
      static struct fruit_message_t{
        const bluetooth_header header = bluetooth_header::fruit_status;
        fruit_status status;
      }fruit_message;
      fruit_message.status = check_fruit();
      Serial.println("Sending fruit status message: " + String(fruit_message.status == fruit_status::ripe ? "ripe" : "unripe"));
      SerialNina.write((byte*)(void*)&fruit_message, sizeof(fruit_message_t));
      break;
    }
    
    case bluetooth_header::find_fruit:
    {
      static struct robot_displacement_t{
        int16_t signed_angle; // degrees
        uint16_t distance; // mm
      } robot_displacement;
      
      SerialNina.readBytes((char*)(void*)&robot_displacement, sizeof(robot_displacement));
      static_assert(sizeof(robot_displacement) == 2 + 2 && offsetof(robot_displacement_t, distance) == 2);
      Serial.println("Recieved find line command with angle " + String(robot_displacement.signed_angle) + " and distance " + String(robot_displacement.distance));
      find_fruit(robot_displacement.signed_angle, robot_displacement.distance);      
      break;
    }
    
    case bluetooth_header::find_line:
    {
      
      struct robot_displacement_t{
        int16_t signed_angle; // degrees
        uint16_t distance; // mm
      } robot_displacement;
      SerialNina.readBytes((char*)(void*)&robot_displacement, sizeof(robot_displacement));
      static_assert(sizeof(robot_displacement) == 2 + 2 && offsetof(robot_displacement_t, distance) == 2);
      Serial.println("Recieved find line command with angle " + String(robot_displacement.signed_angle) + "and distance " + String(robot_displacement.distance));
      find_line(robot_displacement.signed_angle, robot_displacement.distance);      
      
      SerialNina.write((byte)bluetooth_header::line_status);
      SerialNina.write((byte)line_status::on_line);
      break;
    }

    case bluetooth_header::go_to_start:
      go_to_start();
    
    default:
      // Something has gone wrong. Maybe the headers defined in Python don't match the ones here 
      Serial.print("Unknown header: "); Serial.println((int)header);
  }
}
