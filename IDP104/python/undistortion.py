import cv2
import numpy as np
import table_corners
import video_streaming
_npzfile = np.load("cache.npz")
mtx = _npzfile['mtx']

dist = _npzfile['dist']
def undistort(img):
    h,  w = img.shape[:2]
    newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),0.35,(w,h))
    # undistort
    return cv2.undistort(img, mtx, dist, None, newcameramtx)

# The image point must be the point in the original image just after undistort is called
# If the image has been morphed, an inverse must be performed to find the point in the original undistorted image
def _get_world_coordinate(img_point, rotation_mtx, t_vec, camera_mtx, z=0.0):
    t_vec = t_vec.T[0]
    uv_point = np.array([img_point[0],img_point[1], 1.0]).T
    M1 = np.dot((rotation_mtx.T @ np.linalg.inv(camera_mtx)), uv_point)
    M2 = np.dot(rotation_mtx.T, t_vec)
    s = (z + M2[2])/M1[2]
    ret = np.dot(rotation_mtx.T, (s * np.dot(np.linalg.inv(camera_mtx), uv_point) - t_vec))
    return ret

def get_world_coordinate(img_point, z=0.0):
    corners = table_corners.get_corners()
    if len(corners) != 4: return None
    horizontal_sort = sorted(corners, key = lambda x: x[0])
    left = sorted(horizontal_sort[:2], key = lambda y: y[1])
    right = sorted(horizontal_sort[2:], key = lambda y: y[1])
    
    object_top_left = np.float32([0.0, 2430.0, 93.0]) # 9.3 cm above the table
    object_bottom_left = np.float32([0.0, 0, 93.0])
    object_top_right = np.float32([2430.0, 2430.0, 93.0])
    object_bottom_right = np.float32([2430.0, 0.0, 93.0])

    image_top_left = left[0]
    image_bottom_left = left[1]
    image_top_right = right[0]
    image_bottom_right = right[1]
    image_float_corners = np.float32([image_top_left, image_top_right, image_bottom_right, image_bottom_left])
    object_float_corners = np.float32([object_top_left, object_top_right, object_bottom_right, object_bottom_left])
    ret, rvecs, tvec = cv2.solvePnP(object_float_corners, image_float_corners, mtx, np.float32([0]*5))
    if ret:
        rotation_mtx, _ = cv2.Rodrigues(rvecs)
        return _get_world_coordinate(img_point, rotation_mtx, tvec, mtx, z=z)

    

if __name__ == "__main__":
    object_top_left = np.float32([0.0, 2430.0, 93.0]) # 5 cm above the table
    object_bottom_left = np.float32([0.0, 0, 93.0])
    object_top_right = np.float32([2430.0, 2430.0, 93.0])
    object_bottom_right = np.float32([2430.0, 0.0, 93.0])
    while(True):
        corners = table_corners.get_corners()
        if corners is None or len(corners) != 4: 
            continue
        cropped_img = table_corners.get_table_image()
        cv2.imshow('calibresult', cropped_img)
        
        if cv2.waitKey(100) == 27: break