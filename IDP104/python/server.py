# app.py
from flask import Flask, jsonify, request, render_template, Response
import cv2
import json
import bluetooth
from bluetooth_headers import bluetooth_header
import image_processing
import robot_control
app = Flask(__name__)
def gen():
    while True:
        img = image_processing.processed_image
        if img is not None:
            y,x = img.shape[:2]
            x *= 0.8
            y *= 0.8
            img = cv2.resize(img, (round(x), round(y)))
            (flag, encodedImage) = cv2.imencode(".jpg", img)
            yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + 
    			bytearray(encodedImage) + b'\r\n')

def send_message(name: str, **kwargs):
    name_to_enum = {
                    "stop": bluetooth_header.STOP,
                    "follow-line": bluetooth_header.FOLLOW_LINE,
                    "check-fruit": bluetooth_header.CHECK_FRUIT,
                    "find-fruit": bluetooth_header.FIND_FRUIT,
                    "find-line": bluetooth_header.FIND_LINE,
                    }
    enum = name_to_enum[name]
    if enum in [bluetooth_header.STOP, bluetooth_header.FOLLOW_LINE, bluetooth_header.CHECK_FRUIT]:
        bluetooth.send_message(enum)
        
    else:
        bluetooth.send_message(enum, distance=kwargs["distance"],angle=kwargs["angle"])

@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        if len(request.form) == 1:
            name = tuple(request.form.keys())[0]
            send_message(name)
        else:
            # at least for now, this will only be a distance and an angle
            if "find-fruit" in request.form:
                distance = int(request.form["distance"])
                angle = int(request.form["angle"])
                send_message("find-fruit", distance=distance, angle=angle)
            elif "find-line" in request.form:
                distance = int(request.form["distance"])
                angle = int(request.form["angle"])
                send_message("find-line", distance=distance, angle=angle)
            else:
                print(request.form)
                
        return render_template('index.html')
    else:
        return render_template("index.html")


    

@app.route('/video_stream')
def video_stream():
    return Response(gen(),
		mimetype = "multipart/x-mixed-replace; boundary=frame")

@app.route('/commands', methods=['GET', 'POST'])
def hello():

    # POST request
    if request.method == 'POST':

        if request.get_json() != None:

            msg = request.get_json()
            key_list = [i for i in msg.keys()]

            if key_list[0] == 'toggle_streaming':

                toggle_streaming = msg['toggle_streaming']
                print('streaming: ' +str(toggle_streaming))

                with open('data.json', 'r+') as f:
                    data = json.load(f)
                    data['toggle_streaming'] = str(toggle_streaming)
                    f.seek(0)        # <--- should reset file position to the beginning.
                    json.dump(data, f, indent=4)
                    f.truncate()     # remove remaining part

            elif key_list[0] == 'power_on':

                power_on = msg['power_on']
                print('power on: ' +str(power_on))

                with open('data.json', 'r+') as f:
                    data = json.load(f)
                    data['power_on'] = str(power_on)
                    f.seek(0)        # <--- should reset file position to the beginning.
                    json.dump(data, f, indent=4)
                    f.truncate()     # remove remaining part

        return render_template('index.html')

    # GET request
    else:
        return render_template('index.html')


if __name__ == "__main__":
    image_processing.process_image_in_background.start()
    robot_control.robot_control_in_background.start()
    app.run(debug=False)
    image_processing.running = False
    image_processing.process_image_in_background.join()
    robot_control.robot_control_in_background.join()