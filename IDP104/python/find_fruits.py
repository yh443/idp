import numpy as np
import cv2
import time
import enum
import table_corners
import video_streaming
import threading
import undistortion
import math
class fruit_status(enum.Enum):
    RIPE = 0
    UNRIPE = 1        
    UNKNOWN = 2
_id = 1
_active_fruit_id = _id
class fruit:
    def __init__(self):
        global _id
        self.status = fruit_status.UNKNOWN
        self.img_position = None
        self.collected = False
        self.lost = False
        self.id = _id
        _id += 1
    def update_position(self, new_positions: list):
        max_distance2 = 2.0 * 2.0 # pixels
        self.lost = True
        for position in new_positions:
            position = np.float32(list(position))
            if np.sum(np.square(position - self.img_position)) < max_distance2:
                self.img_position = position
                self.lost = False
                return True
        return False
    @property
    def world_position(self):
        return undistortion.get_world_coordinate(self.img_position, z=20.0)
    def __repr__(self):
        return f"fruit #{self.id}, status: {str(self.status).replace('fruit_status.', '')}, position ({self.img_position[0]:.2f}, {self.img_position[1]:.2f}) {', LOST' if self.lost else '' }"
    def __str__(self):
        return f"fruit #{self.id}" + (" (active)" if self.id == _active_fruit_id else "") + " " + str(self.status).replace('fruit_status.', '') 

"""
It is assumed that the image has already been downscaled, blurred etc.
This function finds the centres of the fruits by simple colour maching
"""
tunnel_centre = None
_fruits = []
first_time = True
_fruits_centre = None
def _find_fruits(save_mask = False, save_contour = False):
    while True:
        global tunnel_centre, _fruits, first_time, _fruits_centre
        # no need to find fruits once they're all found
        if len(_fruits) > 0 and all([f.status != fruit_status.UNKNOWN for f in _fruits]): return
        img = table_corners.get_table_image()
        
        # define range of red color in HSV (more useful than RGB in this case)
        lower_red = np.array([160, 100, 100])
        upper_red = np.array([180,190,230])
        
        # define range of yellow color in HSV (more useful than RGB in this case)
        lower_yellow = np.array([30, 100, 120])
        upper_yellow = np.array([40,255,255])

        # make a copy of the image but in the HSV colour space
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        
        if tunnel_centre is None or tunnel_centre == img.shape[:2]:
            tunnel_mask = cv2.inRange(hsv, lower_yellow, upper_yellow)
            tunnel_contours, _ = cv2.findContours(tunnel_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
            # create a region of interest to the left of the tunnel
            # we know that the fruits will be to the left of the tunnel so
            # this will speed up the search and reduce false positives
            if(len(tunnel_contours) > 0):
                tunnel_contour =  max(tunnel_contours, key=lambda contour : cv2.contourArea(contour))
                tunnel_moment = cv2.moments(tunnel_contour)
                tunnel_centre = (tunnel_moment['m10'] / (tunnel_moment['m00'] + 1e-5), tunnel_moment['m01'] / (tunnel_moment['m00'] + 1e-5))
            else:
                # If no tunnel could be found
                tunnel_centre = img.shape[:2] # search the whole image
        if int(tunnel_centre[0]) <= 0: continue
        hsv_fruit_roi =  hsv[:,:int(tunnel_centre[0])]
        # Threshold the HSV image to get only red colors
        # This produces a new image where the red pixels are white and all other pixels are black
        fruit_mask = cv2.inRange(hsv_fruit_roi, lower_red, upper_red)
        
        # Find every contour (shape in the new image -- these represent the different fruits)
        fruit_contours, _ = cv2.findContours(fruit_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        
         # Get the moments these contain data such as the area or centre of mass of the contours
        moments = []
        for contour in fruit_contours:
            moment = cv2.moments(contour)
            # make sure the contour isn't just noise less than 10  pixels in area
            MINIMUM_AREA = 10       # pixels
            if(moment['m00'] > MINIMUM_AREA):
                moments.append(moment)
        
        centres_of_fruits = [None]*len(moments)
        for i in range(len(moments)):
            # add 1e-5 to avoid division by zero
            centres_of_fruits[i] = (moments[i]['m10'] / (moments[i]['m00'] + 1e-5), moments[i]['m01'] / (moments[i]['m00'] + 1e-5))
        if save_mask:
            cv2.imwrite("fruit-mask.jpg", fruit_mask)
        if first_time and len(centres_of_fruits) == 6:
            temp_fruits = []
            first_time = False
            fruits_centre = np.mean(np.float32(centres_of_fruits), axis = 0)
            img[int(fruits_centre[1]), int(fruits_centre[0])] = 255
            tunnel_vector = tunnel_centre - fruits_centre
            _fruits_centre = fruits_centre
            
            
            def angle(v1, v2):
                det = v1[0] * v2[1] - v2[0] * v1[1]
                dot = v1 @ v2
                _angle = math.atan2(det, dot)
                return _angle if _angle >= 0.0 else _angle + 2 * math.pi   

            centres_of_fruits = sorted(centres_of_fruits, key=lambda centre: angle(centre - np.float32(fruits_centre), tunnel_vector))
            #print(centres_of_fruits)
            for point in centres_of_fruits:
                f = fruit()
                f.img_position =np.float32(list(point))
                temp_fruits.append(f)
            _fruits = temp_fruits
        else:
            for f in _fruits:
                f.update_position(centres_of_fruits)

_t = threading.Thread(target=_find_fruits, daemon=True)
_t.start()

def get_fruits():
    global _fruits
    return _fruits

def set_fruit_status(status: fruit_status):
    global _active_fruit_id
    fruits = get_fruits()
    for f in _fruits:
        if f.id == _active_fruit_id:
            f.status = status
            if status == fruit_status.RIPE:
                f.collected = True
            break
    _active_fruit_id += 1

def get_active_fruit():
    for f in _fruits:
        if f.id == _active_fruit_id:
            return f

if __name__ == "__main__":
    
    first_time = True
    i=0
    while(True):
        t0 = time.time()
        corners = table_corners.get_corners()
        while corners is None or len(corners) != 4: corners = table_corners.get_corners()
        img = table_corners.get_table_image()
        for corner in corners:
            x,y = corner.ravel()
            img[int(y), int(x)] = 255
        # apply an edge preserving filter (quite time consuming)
        grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        fruits = get_fruits()
        for f in fruits:
            img[int(f.img_position[1]), int(f.img_position[0])] = 255
            col = cv2.cvtColor(np.uint8([[[f.id * 256//len(fruits), 255, 255]]]), cv2.COLOR_HSV2BGR)[0][0]
            b,g,r = col.ravel()
            b = int(b)
            g = int(g)
            r = int(r)
            cv2.putText(img, str(f), (round(f.img_position[0]),round(f.img_position[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (b,g,r))
        ESCAPE_KEY = 27
        if(cv2.waitKey(100) == ESCAPE_KEY): break
        cv2.imshow("Image", img)
        i+=1
        if i % 100 == 0: set_fruit_status(fruit_status.RIPE)