# This folder is for the image recognition and the Bluetooth interface

### dependencies
- pip install OpenCV-python numpy flask
- python 3.6+ (2018)

## find_fruits.py
This is the file that has a function for fruit detection  

### notes
- It uses colour detection to find the fruits
- It should be robust to reasonable changes in lighting 
- It will probably fail under drastic changes in lighting
- It will probably fail if very similar colours are found which are not fruits

## find_corners.py
This is the file that has a function to detect the corners of the table

### notes
- This function assumes most of the pixels on the table's boundary are a shade of gray
- It creates a mask of all of the grey pixels
  - Grey is defined as a colour with R, G and B channels all within 15 of each other
- The corners on this specific picture are correct to within a few pixels
- It's hard to say how reliable this would be - it would probably fail if there are grey pixels at the boundary of the table which don't belong to the table

## Web UI
- Written with HTML and JavaScript.
- The UI is currently under development.
- It is expected to control the video streaming and turn on & off robots.
- More functions can be added.

## image_processing.py
This file:
- reads the stream of the table
- finds the corners of the table
- Morphs the image so the table's corners map to the corners of the output image
- finds all the fruits to the left of the tunnel within the table

## bluetooth.py
Provides functions for sending messages via bluetooth