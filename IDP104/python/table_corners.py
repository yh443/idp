import numpy as np
import cv2
import time
import video_streaming
import threading
'''
It is assumed that the image has aready been blurred, downscaled etc.
This returns an array of points (x,y) on the image 
'''
last_mask = None
first_time = True
i = 0
corners_array = []
cached_highp_corners = None
_highp_corners = np.float32([
 [864.32715,  654.2733  ],
 [185.97252,  733.7925  ],
 [151.47086,   77.4355  ],
 [770.1715,    21.470297]
 ])

_npzfile = np.load("cache.npz")
mtx = _npzfile['mtx']
dist = _npzfile['dist']

def undistort(img):
    h,  w = img.shape[:2]
    newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),0.35,(w,h), cv2.CALIB_FIX_PRINCIPAL_POINT)
    # undistort
    return cv2.undistort(img, mtx, dist, None, newcameramtx)

_img = None
_grey = None
def _find_corners(eps = 0.02, save_mask = False, draw_contour = False):
    global first_time, last_mask, i, corners_array, cached_highp_corners, _highp_corners, _img, _frame
    while cached_highp_corners is None:
        if len(corners_array) <= 50:
            # there isn't a guarantee that these images correspond to the same frame but they probably do
            # the corners shouldn't move between frames anyways            
            _grey = video_streaming.get_grey()
            _img = video_streaming.get_frame()
            _img = cv2.bilateralFilter(_img,9,75,75)
            if _grey is None or _img is None: continue
            _grey = undistort(_grey)
            _img = undistort(_img)
            # get a grey mask. This should get most of the table
            # the r, g and b values must all be within 15 / 256
            # cast the image to signed 16 bit integers to stop overflow
            # this is a very fast operation
            wide_img = _img.astype(np.int16)
            threshold = 17
            bg = (np.abs(wide_img[:,:,0] - wide_img[:,:,1]) < threshold).astype(np.uint8)*255
            gr = (np.abs(wide_img[:,:,1] - wide_img[:,:,2])  < threshold).astype(np.uint8)*255
            rb = (np.abs(wide_img[:,:,2] - wide_img[:,:,0])  < threshold).astype(np.uint8)*255
            mask = bg & gr
            mask = mask & rb & np.abs(wide_img[:,:,0] + wide_img[:,:,1] + wide_img[:,:,2] < 200).astype(np.uint8)*255
            if not first_time:
                #mask = np.uint8(np.clip(np.float32(last_mask) + 0.1 * (np.float32(mask) - np.float32(last_mask)), 0.0, 255.0))
                last_mask = mask
            else:
                last_mask = mask
                #first_time = False
            # find the contours
            contours, hierarchy = cv2.findContours(mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

            # assume the table is the biggest contour
            table_contour = max(contours, key=lambda contour : cv2.contourArea(contour))
            if draw_contour:
                # draw a green contour on the image around the table
                cv2.drawContours(_img, [table_contour,], 0, (0,255,0), 1)

            # 0.15 to 0.02 seem to work
            epsilon = eps*cv2.arcLength(table_contour,True)

            # convert the high resolution contour to a quadrelateral (provided a good choice of epsilon was used)
            table_contour = cv2.approxPolyDP(table_contour,epsilon,True)

            # get the points on the contour
            hull = cv2.convexHull(table_contour)
            #cv2.waitKey(1)
            if save_mask:
                cv2.imwrite("./grey-mask.jpg", mask)
            corners = hull[:,0]
            ACCURACY = 0.01 # pixels
            MAX_ITTER = 30 
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, MAX_ITTER, ACCURACY)
            temp_highp_corners = cv2.cornerSubPix(_grey, corners.astype(np.float32), (21,21), (-1,-1), criteria)
            if temp_highp_corners is not None and len(temp_highp_corners) == 4:
                _highp_corners = temp_highp_corners
            else:
                print("Can't find 4 corners. Try moving away from the table\a")
                continue
            # cv2.imshow("mask", mask)
            #print(len(_highp_corners))
            if len(_highp_corners) == 4:
                corners_array.append(_highp_corners)
            if len(corners_array) == 50:
                cached_highp_corners = np.median(corners_array, axis=0)
                print("found all corners")

def get_corners():
    return np.float32([
     [864.32715,  654.2733  ],
     [185.97252,  733.7925  ],
     [151.47086,   77.4355  ],
     [770.1715,    21.470297]
     ])#cached_highp_corners if cached_highp_corners is not None else _highp_corners

_table_image = None
def _retrieve_table():
    while(True):
        global _table_image
        _img = undistort(video_streaming.get_frame())
        _img = cv2.bilateralFilter(_img,9,75,75)
        corners = get_corners()
        # this produces an infinite loop if 4 corners can't be found
        while corners is None or len(corners) != 4: corners = get_corners()
        #if len(corners) != 4: return _img
        border_width = 0 # px
        top_right = np.max(corners, axis=0).astype(np.int32) + np.int32([border_width, border_width])
        bottom_left = np.min(corners, axis=0).astype(np.int32) - np.int32([border_width, border_width])
        mask = np.zeros(_img.shape, dtype=np.uint8)
        mask[bottom_left[1]:top_right[1],bottom_left[0]:top_right[0]:] = 255
        _table_image = _img & mask
def get_table_image():
    global _table_image
    while _table_image is None: 
        print("waiting for table image")
        time.sleep(0.1)
    return _table_image.copy()

#_corner_thread = threading.Thread(target=_find_corners)
#_corner_thread.start()
_retrieve_table_thread = threading.Thread(target=_retrieve_table)
_retrieve_table_thread.start()
if __name__ == "__main__":
    while(True):
        img =  get_table_image()
        cv2.imshow("Image",img)
        cv2.waitKey(1)