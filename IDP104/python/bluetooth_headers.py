import enum
# these values define the type of message that is being sent
class bluetooth_header(enum.IntEnum):
    STOP = 0
    FOLLOW_LINE = 1
    CHECK_FRUIT = 2
    FIND_FRUIT = 3
    FIND_LINE = 4
    
    CHECK_POSE = 5
    LINE_STATUS = 6
    FRUIT_STATUS = 7
    GO_TO_START = 8
    
class line_status(enum.IntEnum):
    ON_LINE = 0
    OFF_LINE = 1