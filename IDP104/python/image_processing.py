from table_corners import get_corners, get_table_image
from find_fruits import get_fruits, get_active_fruit
from find_robot import robot_tracking
import cv2
import time
import numpy as np
import json
import threading
import undistortion
import video_streaming
import math
import bluetooth
# use this variable to get the cropped image
processed_image = None
running = True



def _process_image():
    global processed_image, running
    while(running):        
        _processed_image = get_table_image()
        if _processed_image is None: continue
        
        float_corners = get_corners()
        
        if float_corners is None: continue
        
        corners = float_corners.astype(np.int32)
        try:
            fruits = get_fruits()
            
        except Exception as e:
            print(e)
            fruits = np.array([])

        # make a 9 pixel blue square at the centre of the fruits
        for fruit in fruits:
            x = int(fruit.img_position[0])
            y = int(fruit.img_position[1])
            pos = undistortion.get_world_coordinate(fruit.img_position, z=20.0)
            if pos is not None:
                _processed_image[y-1:y+2, x-1:x+2] = [255,0,0]
                world_x, world_y, world_z = pos.ravel()
                cv2.putText(_processed_image,str(fruit), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 0.35, [200,200,0])
                cv2.putText(_processed_image, f"({world_x:.0f}, {world_y:.0f}, {world_z:.0f})", (x,y+15), cv2.FONT_HERSHEY_SIMPLEX, 0.35, [200,200,0])

        # make a 9 pixel red square at the corners of the table
        for corner in corners:
            x = corner[0]
            y = corner[1]
            cv2.circle(_processed_image, (x,y), 7, [0,0,255], 1)
        
        _processsed_image, position, orient = robot_tracking(_processed_image)
        if len(position) == 2:
            direction = (get_active_fruit().world_position - undistortion.get_world_coordinate(position))[:2]
            distance = np.linalg.norm(direction)
            image_direction = get_active_fruit().img_position - position
            angle = math.degrees(math.atan2(image_direction[1], image_direction[0])) - orient
            while angle < 180: angle += 360
            while angle > 180: angle -= 360
            cv2.putText(_processed_image, f"({distance:.0f} mm, {angle:.0f} deg)", (int(position[0]), int(position[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, [0,0,255])
        processed_image = _processed_image

process_image_in_background = threading.Thread(target = _process_image)
if __name__ == "__main__":
    process_image_in_background.start()

    while True:
        
        while processed_image is None: continue
        cv2.imshow("Cropped-image", processed_image)
        if cv2.waitKey(10) == 27: break

    running = False
    process_image_in_background.join()
