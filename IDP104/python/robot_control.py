from table_corners import get_corners, get_table_image
from find_fruits import get_fruits, get_active_fruit, set_fruit_status, fruit_status
from find_robot import robot_tracking
from bluetooth_headers import bluetooth_header, line_status
import cv2
import time
import numpy as np
import json
import threading
import undistortion
import video_streaming
import math
import bluetooth
""" 
this enters an infinite loop until the power is turned on
once power is on, it can't be turned off again programatically
"""
def power_switch():
    print("[POWER SWITCH]: ATTEMPTING TO SEND STOP COMMAND")
    bluetooth.send_message(bluetooth_header.STOP) # tell the robot to stop
    power_on = 0
    # Enter a loop which will end once 'power_on' is set to anything integer but 0
    while power_on == 0:
        with open('data.json', 'r+') as f:
            data = json.load(f)
            power_on = int(data['power_on'])
            f.seek(0)        # <--- should reset file position to the beginning.
            json.dump(data, f, indent=4)
            f.truncate()     # remove remaining part
    print("[POWER SWITCH]: POWER ON")

"""
This goes to the tunnel and stops at the exit
"""
def go_to_tunnel():
    _processed_image = get_table_image()
    print("[GO TO TUNNEL]: ATTEMPTING TO SEND FOLLOW_LINE COMMAND")
    bluetooth.send_message(bluetooth_header.FOLLOW_LINE)
    exit_position = None
    exit_orient = None
    while not (type(exit_position) == tuple and type(exit_orient) == float): 
        _processed_image = get_table_image()
        _tunnel_exit = _processed_image[300:430, 390:500]
        _tunnel_exit, exit_position, exit_orient = robot_tracking(_tunnel_exit)
        cv2.imshow("tunnel-exit", _tunnel_exit)
        cv2.waitKey(1)
    cv2.destroyAllWindows()
    print("[GO TO TUNNEL]: FOUND ROBOT AT TUNNEL EXIT")
    print("[GO TO TUNNEL]: ATTEMPTING TO SEND STOP COMMAND")
    #tell arduino to stop following the line
    bluetooth.send_message(bluetooth_header.STOP)

"""
This goes to the current active fruit and stops 5.5 cm away
"""
def go_to_active_fruit():
    active_fruit_image_position = get_active_fruit().img_position
    active_fruit_world_position = get_active_fruit().world_position
    
    distance = angle = float("inf")
    while distance > 50 or abs(angle) > 5:
        print("[GO TO ACTIVE FRUIT]: ATTEMPTING TO SEND STOP COMMAND")
        bluetooth.send_message(bluetooth_header.STOP)
        time.sleep(2)
        _processed_image = get_table_image()
        
        _tunnel_exit, exit_position, exit_orient = robot_tracking(_processed_image)
        print(len(exit_position))
        if len(exit_position) == 2:
            print(exit_position)
            world_direction = (active_fruit_world_position - undistortion.get_world_coordinate(exit_position, z=100.0))[:2]
            image_direction = np.float64(active_fruit_image_position) - exit_position
            angle = -(math.degrees(np.arctan2(image_direction[1], image_direction[0])) - exit_orient) ## in image space
            while angle < -180: angle += 360
            while angle > 180: angle -= 360
            distance_to_fruit = np.linalg.norm(world_direction)
            distance = distance_to_fruit - (100 + 250)
            # if you're within 13 cm of the fruit, go 5.5 cm away
            if distance_to_fruit < 130 + 250:
                print(distance_to_fruit)
                distance = max( distance_to_fruit - (50 + 250), 0)
            print(distance_to_fruit, angle)
            #distance, angle = 0,0
            print(f"[GO TO ACTIVE FRUIT]: ATTEMPTING TO SEND FIND_FRUIT COMMAND {'{'}{angle:.0f}°, {distance:.0f} mm{'}'}")
            bluetooth.send_message(bluetooth_header.FIND_FRUIT, distance=distance , angle=angle)
            print("[GO TO ACTIVE FRUIT]: WAITING FOR ARDUINO TO RESPOND")
            t0 = time.time()
            while not bluetooth.available(): continue
            if bluetooth.available():
                msg = bluetooth.get_message()
                assert msg[0] == bluetooth_header.CHECK_POSE
            print("[GO TO ACTIVE FRUIT]: RECIEVED CHECK POSE COMMAND")

"""
When the robot is 5.5 cm away from the active fruit,
this sends a command to check the active fruit, collect it if ripe and avoid if unripe
It then updates the active fruit with this information
"""
def check_fruit():
    print("[CHECK FRUIT]: ATTEMPTING TO SEND CHECK_FRUIT COMMAND")
    bluetooth.send_message(bluetooth_header.CHECK_FRUIT)
    while not bluetooth.available(): continue
    msg = bluetooth.get_message()
    assert msg[0] == bluetooth_header.FRUIT_STATUS
    print(f"[CHECK FRUIT]: RECIEVED CHECK FRUIT STATUS MESSAGE (STATUS: {msg[1]})")
    set_fruit_status(msg[1])

"""
This goes 5 cm away from all of the fruits and sets them all to RIPE
It doesn't do any collection or avoidance
"""    
def go_to_all_fruits():
    for i in range(6):
        go_to_active_fruit()
        set_fruit_status(fruit_status.RIPE)

"""
This collects all of the ripe fruits and avoids all unripe fruits
"""
def collect_ripe_fruits():
    for i in range(6):
        go_to_active_fruit()
        check_fruit()
"""
This takes the robot to the line just before the tunnel
"""
def find_line_to_tunnel():
    _processed_image = get_table_image()
    # 2 points on the line
    image_point1 = np.float64([458, 358])
    #image_point2 = np.float64([425, 360])    
    world_point1 = undistortion.get_world_coordinate(image_point1)
    on_line = False
    while not on_line:
        _processed_image = get_table_image()
        _, position, orient = robot_tracking(_processed_image)
        robot_direction = undistortion.get_world_coordinate(position) - world_point1
        robot_image_direction = image_point1 - position
        distance = np.linalg.norm(robot_direction) * 0.8
        robot_angle = -(math.degrees(math.atan2(robot_image_direction[1], robot_image_direction[0])) - orient)
        print(f"[FIND LINE TO TUNNEL]: ATTEMPTING TO SEND FIND LINE {'{'}{robot_angle:.0f}°, {distance:.0f} mm{'}'}")
        bluetooth.send_message(bluetooth_header.FIND_LINE, distance=max(distance - 250, 0), angle = robot_angle)
        print("[FIND LINE TO TUNNEL]: WAITING FOR ARDUINO TO RESPOND")
        while not bluetooth.available(): continue
        msg = bluetooth.get_message()
        assert msg[0] == bluetooth_header.LINE_STATUS
        print(F"[FIND LINE TO TUNNEL]: RECIEVED LINE STATUS MESSAGE COMMAND ({str(msg[1])})")
        on_line = line_status(msg[1]) == line_status.ON_LINE

"""
This follows the line to the dropoff point and stops when the robot gets there
"""
def follow_line_to_dropoff():
    _processed_image = get_table_image()
    print("[FOLLOW LINE TO DROPOFF]: ATTEMPTING TO SEND FOLLOW_LINE COMMAND")
    bluetooth.send_message(bluetooth_header.FOLLOW_LINE)
    robot_position = None
    robot_orient = None
    while not (type(robot_position) == tuple and type(robot_orient) == float): 
        _processed_image = get_table_image()
        _drop_off_point = _processed_image[480-50:480+50, 700-50:700+50]
        _drop_off_point, robot_position, robot_orient = robot_tracking(_drop_off_point)
        cv2.imshow("drop-off-point", _drop_off_point)
        cv2.waitKey(1)
    cv2.destroyAllWindows()
    print("[FOLLOW LINE TO DROPOFF]: ATTEMPTING TO SEND STOP COMMAND")
    #tell arduino to stop following the line
    bluetooth.send_message(bluetooth_header.STOP)
    # move the robot forward 15 cm
    bluetooth.send_message(bluetooth_header.FIND_FRUIT, distance=150, angle=0)
    while not bluetooth.available(): continue
    bluetooth.get_message()
    bluetooth.send_message(bluetooth_header.STOP)

def go_to_start():
    bluetooth.send_message(bluetooth_header.GO_TO_START)
    at_start = False
    while not at_start:
        _processed_image = get_table_image()[0:140,700-70:700+70]
        cv2.imshow("img",_processed_image)
        cv2.waitKey(1)
        _, position, orient = robot_tracking(_processed_image)
        at_start = type(position) == tuple and type(orient) == float
    bluetooth.send_message(bluetooth_header.STOP)
        
        # robot_direction = undistortion.get_world_coordinate(position) - world_point1
        # distance = np.linalg.norm(robot_direction)
        # robot_angle = -(math.degrees(math.atan2(robot_direction[1], robot_direction[0])) - orient)
        # print(f"[FIND LINE TO TUNNEL]: ATTEMPTING TO SEND FIND LINE {'{'}{robot_angle:.0f}°, {distance:.0f} mm{'}'}")
        # bluetooth.send_message(bluetooth_header.FIND_LINE, distance=distance, angle = robot_angle)
        # print("[FIND LINE TO TUNNEL]: WAITING FOR ARDUINO TO RESPOND")
        # while not bluetooth.available(): continue
        # msg = bluetooth.get_message()
        # assert msg[0] == bluetooth_header.LINE_STATUS
        # print(F"[FIND LINE TO TUNNEL]: RECIEVED LINE STATUS MESSAGE COMMAND ({str(msg[1])})")
        # on_line = line_status(msg[1]) == line_status.ON_LINE
        #             
    

def robot_control():
    power_switch() # wait until the button is pressed
    go_to_tunnel() # go to the tunnel and stop at the exit
    go_to_all_fruits() # go to all fruits and collect the ripe ones
    find_line_to_tunnel() # go back to the line at the exit of the tunnel
    follow_line_to_dropoff() # follow the line to the drop-off         
robot_control_in_background = threading.Thread(target = robot_control)

if __name__ == "__main__":
    #power_switch()
    print("___________________________________________________________________")
    t0 = time.time()
    #bluetooth.send_message(bluetooth_header.FOLLOW_LINE)
    #bluetooth.send_message(bluetooth_header.FIND_FRUIT, distance=300, angle=0)
    #go_to_tunnel()
    #print("___________________________________________________________________")
    #go_to_active_fruit()
    #print("___________________________________________________________________")
    check_fruit()
    print("___________________________________________________________________")
    #collect_ripe_fruits()
    #print("___________________________________________________________________")
    #find_line_to_tunnel()
    #print("___________________________________________________________________")
    follow_line_to_dropoff()
    print("___________________________________________________________________")
    go_to_start()
    t = int(time.time() - t0)
    print(f"TIME TAKEN: {t//60:02d}:{t%60:02d}")