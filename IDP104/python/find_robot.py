import numpy as np
import math, time
import cv2
import undistortion
import video_streaming
import table_corners

def robot_tracking(img):



    #range of pink and blue in HSV
    '''
    lower_orange = np.array([0,130,90])
    upper_orange = np.array([10,180,255])
    lower_blue = np.array([100, 240, 90])
    upper_blue = np.array([110, 255, 255])
    '''
    #lower_pink = np.array([140,50,180])
    lower_pink = np.array([140,30,180])
    upper_pink = np.array([160,80,255])
    lower_blue = np.array([100,120,100])
    upper_blue = np.array([120,255,255])

    #convert img to hsv and define a blur function
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)[:,:560]
    
    #masks for each colour
    blue_mask = cv2.inRange(hsv_img, lower_blue, upper_blue)
    pink_mask = cv2.inRange(hsv_img, lower_pink, upper_pink)
    # 
    #cv2.waitKey(1)
    #cv2.imshow("blue", blue_mask)
    #cv2.imshow("pink", pink_mask)
    cv2.waitKey(10)

    blue_contours, _ = cv2.findContours(blue_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    pink_contours, _ = cv2.findContours(pink_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    

    blue_centre = None
    pink_centre = None
    found_blue = False
    found_pink = False

    for contour in blue_contours:
        moment = cv2.moments(contour)
        approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)
        if (moment['m00'] > 10 and moment['m00'] < 1000 and (len(approx) > 8) and (len(approx) < 25)):
            found_blue = True
            blue_centre = (moment['m10'] / (moment['m00'] + 1e-5), moment['m01'] / (moment['m00'] + 1e-5))
            break

    for contour in pink_contours:
        moment = cv2.moments(contour)
        approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)
        if (moment['m00'] > 10 and moment['m00'] < 1000 and (len(approx) > 8) and (len(approx) < 25)):
            found_pink = True
            pink_centre = (moment['m10'] / (moment['m00'] + 1e-5), moment['m01'] / (moment['m00'] + 1e-5))
            break

        
    if (found_blue == True and found_pink == True ):
        #and ((blue_centre[1]-orange_centre[1])**2 + (blue_centre[0]-orange_centre[0])**2)<40000
        #print('robot found')
        ## in image space
        position = pink_centre
        #blue_centre = undistortion._get_world_coordinate(blue_centre, z = 20.0)
        img[int(blue_centre[1]-2) : int(blue_centre[1]+2), int(blue_centre[0]-2) : int(blue_centre[0]+2)] = [0,255,0]
        img[int(pink_centre[1]-2) : int(pink_centre[1]+2), int(pink_centre[0]-2) : int(pink_centre[0]+2)] = [0,255,0]
        direction = np.float64(blue_centre) - pink_centre
        #pink_centre = undistortion._get_world_coordinate(pink_centre, z = 20.0)
        orient = math.degrees(np.arctan2(direction[1], direction[0])) ## in image space
    else:
        position = []
        orient = []
    return img, position, orient

    

if __name__ == "__main__":

    while(True):
        img, position, orient = robot_tracking(table_corners.get_table_image())
        cv2.imshow('robot', img)
        if len(position) == 2:
            print("robot found at", np.round(undistortion.get_world_coordinate(np.float64(position), z=150)), f"pointing  {orient:.1f} degrees clockwise from the horizontal in image space")
        if(cv2.waitKey(100) == 27): break
    print("camera shut down")
    capture.release()

    cv2.destroyAllWindows()
