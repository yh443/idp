import serial # for the bluetooth connection
from bluetooth_headers import bluetooth_header, line_status
from find_fruits import fruit_status
ser = None
i = 0
port = "COM3"
try:
    # connect to the serial port
    # the baud rate must match the one in the arduino program for  SerialNina
    ser = serial.Serial(port, 57600, timeout=1.0)
    #ser = serial.Serial(port, 115200, timeout=1.0)
    print("bluetooth connection started")
except Exception as e:
    print(e)
    print(ser)
    
"""
examples:
send_message( bluetooth_header.STOP)
send_message( bluetooth_header.FOLLOW_LINE)
send_message( bluetooth_header.CHECK_FRUIT)
send_message( bluetooth_header.FIND_FRUIT, int(distance), int(signed_angle))
send_message( bluetooth_header.FIND_LINE, int(distance), int(signed_angle))
"""
def send_message(header: bluetooth_header, **kwargs):
    if ser is None:
        print(f'message ({str(header) + (", " + str(kwargs) if kwargs else "")}) not sent - serial unavailable')
        return
    if header == bluetooth_header.STOP:
        assert len(kwargs) == 0
        ser.write(bytes([bluetooth_header.STOP]))
        #ser.write(bytes([bluetooth_header.STOP]))

    elif header == bluetooth_header.FOLLOW_LINE:
        assert len(kwargs) == 0
        ser.write(bytes([bluetooth_header.FOLLOW_LINE]))
    
    elif header == bluetooth_header.CHECK_FRUIT:
        assert len(kwargs) == 0
        ser.write(bytes([bluetooth_header.CHECK_FRUIT]))
    
    elif header == bluetooth_header.GO_TO_START:
        assert len(kwargs) == 0
        ser.write(bytes([bluetooth_header.GO_TO_START]))
    
    elif header == bluetooth_header.FIND_FRUIT:
        assert len(kwargs) == 2
        distance = kwargs["distance"] * 5/9
        signed_angle = kwargs["angle"]
        signed_angle = int(signed_angle)
        distance = int(distance)
        ser.write(bytes([bluetooth_header.FIND_FRUIT, *signed_angle.to_bytes(2, signed=True, byteorder = "little"), *distance.to_bytes(2, signed=False, byteorder = "little")]))
        
        
    elif header == bluetooth_header.FIND_LINE:
        assert len(kwargs) == 2
        distance = kwargs["distance"]
        signed_angle = kwargs["angle"]
        signed_angle = int(signed_angle)
        distance = int(distance)
        ser.write(bytes([bluetooth_header.FIND_LINE, *signed_angle.to_bytes(2, signed=True, byteorder = "little"), *distance.to_bytes(2, signed=False, byteorder = "little")]))
    else:
        # something wrong has happened that needs to be found
        assert False

def get_message():
    if not ser.inWaiting(): return None
    else:
        header = int(ser.read(1)[0])
        if header == bluetooth_header.CHECK_POSE:
            return bluetooth_header.CHECK_POSE,
        elif header == bluetooth_header.LINE_STATUS:
            return bluetooth_header.LINE_STATUS, line_status(int(ser.read(1)[0]))
        elif header == bluetooth_header.FRUIT_STATUS:
            return bluetooth_header.FRUIT_STATUS, fruit_status(int(ser.read(1)[0]))
def available():
    return ser.inWaiting()