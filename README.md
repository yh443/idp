# IDP

This is the IDP directory for group 104.

C++ AND ARDUINO FILES ARE PLACED IN "ARDUINO" FOLDER.
PYTHON FILES AND WEB UI ARE PLACED IN "PYTHON" FOLDER.

Developed by Yuan & Elorm.

# Table of Contents
- [Bluetooth](#bluetooth)
- [Fruit detection](#fruit-detection)
- [Web UI](#web-ui)

## Bluetooth

### How it works

- Every message has a **header** and some have a **body**
- The **header** to a message is an unsigned byte which describes the message being sent (both the laptop and the robot must agree on the meanings of the headers)
- The (optional) **body** to the message sends additional information

### List of headers
```cpp
enum class fruit_status: uint8_t{
  ripe = 0,
  unripe
};
enum class bluetooth_header : uint8_t {
  stop = 0, // this has no body and is equivalent to the message {go_forward, (uint8_t)0}
  follow_line, // this has no body but the Arduino must be able to respond timely to a stop command
  check_fruit, // this has no body. It just means that python is happy that the robot is facing the fruit at an acceptable angle and distance
  find_fruit, // followed by {int16_t signed_angle /*in degrees*/, uint16_t distance /*in mm*/}
  find_line, // followed by {int16_t signed_angle /*in degrees*/, uint16_t distance /*in mm*/}
  
  check_position_and_orientation, // not followed by anything, just a request for python to check if the robot is facing the fruit
  line_status, // followed by 1 byte enum (on-line, off-line)
  fruit_status // followed by the fruit's status (1 byte enum (ripe/unripe))
};
```


### Example code

#### .ino file:
```cpp
if(SerialNina.available()){
  static bluetooth_header header;
  // read 1 byte into the variable header
  SerialNina.readBytes(&(uint8_t*)(void*)header, 1);
  switch (header) {
    case bluetooth_header::go_forward:
    {
      // go_forward command is always directly followed by a speed of type uint8_t
      uint8_t speed;
      // get the speed
      SerialNina.readBytes(&speed, sizeof(speed));
      // set the motor speed
      set_motor_speeds(/*left*/speed, /*right*/ speed);
      break;
    }
    ...
  }
}
```
 
#### .py file:
```python
# Python's struct module can be used to send other data types like floats or larger integers
def go_forward(ser: Serial, speed: int):
  assert(0 <= speed <= 255)
  ser.write(bytes([bluetooth_headers.go_forward, speed])) # bluetooth_headers.go_forward == 2
```
 
### Usage notes
- First start the Arduino, then start the python program.
- Make sure the com port is correct.
- If you restart the Arduino, you'll need to restart the serial connection in the python script

## Fruit detection
...

## Web UI
...
